---
title: TypeScript のクエスチョンマーク(?)
date: 2018-09-01
tags:
- TypeScript
sidebar: auto
---

いつも忘れて調べ直してるので、メモとして残します。  
クエスチョンマーク（?）が付いたプロパティはオプション扱い、という意味になります。


```ts
var person: {
    name: string;
    sex: string
    // プロパティに?が付くとオプション扱い
    age?: number;
}
```


### 参考
- [Optional and Default Parameters](https://www.typescriptlang.org/docs/handbook/functions.html#optional-and-default-parameters)



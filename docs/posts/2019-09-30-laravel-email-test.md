---
title: Laravel 5 でテストメール送信環境の設定
date: 2018-09-30T18:30:00+09:00
tags:
- php
- laravel
- mail
sidebar: auto
---

## ローカル環境の場合

`Mailtrap`を利用する

1. https://mailtrap.io/inboxes/ にアクセスする。
1. 任意のユーザー登録をおこなう
1. 「Demo inbox」をクリックした後、「Integrations」で「Laravel」を選択する
1. 個人の「.env」に表示された「username」と「password」を記載する

### 参考

- https://mailtrap.io/inboxes/
- https://qiita.com/Yorinton/items/ec11651fdc79356cac75
- https://readouble.com/laravel/5.6/ja/mail.html

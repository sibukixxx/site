---
title: 【PHP】 ドット付きのオブジェクトプロパティにアクセスする 
date: 2018-09-03T18:30:00+09:00
tags:
- php
- firebase
- jwt
sidebar: auto
---

Firebase から取得した JWT を調査していたところ、  
PHPのObject にドット付きのプロパティが存在していた。  

```
[2018-09-03 17:51:25] local.DEBUG: array (
  0 => 
  stdClass::__set_state(array(
     'google.com' => 
    array (
      0 => '{任意の数値}',
    ),
     'email' => 
    array (
      0 => 'example@example.com',
    ),
  )),
)  
```

通常Objectにアクセスするには、 `$example->property_name` とするが、  
そのままドット付きでアクセスしようとすると当然エラーになってしまう。  
  
調べたところ、アクセスしたいプロパティを `{'property_name'}` とすればいいようだ。
今回の場合は以下のようにすると、期待する値を取得することができた。

```
$firebase->{'google.com'}
```

### 参考
- [php object attribute with dot in name](https://stackoverflow.com/questions/5351018/php-object-attribute-with-dot-in-name)


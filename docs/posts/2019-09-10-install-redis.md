---
title: Ubuntu に Redis をインストールする
date: 2018-09-10T18:30:00+09:00
tags:
- redis
sidebar: auto
---

## インストールと起動停止

```
$ sudo apt update -y
$ sudo apt show redis-server
```

```
$ redis-cli --version
redis-cli 3.0.6
$ redis-server --version
Redis server v=3.0.6 sha=00000000:0 malloc=jemalloc-3.6.0 bits=64 build=687a2a319020fa42
```

起動・停止、自動起動

```
$ sudo systemctl start redis-server
$ sudo systemctl stop redis-server
$ sudo systemctl enable redis-server
```

## 各種設定

- 外部サーバからの接続許可設定

```
$ sed -i -e "s/^bind *127\.0\.0\.1$/bind 0\.0\.0\.0/g" /etc/redis/redis.conf
$ sudo systemctl restart redis-server
```

- パスワード認証追加

```
requirepass {password string}
```

- 確認
  - 事前にredisのportをsecurity groupで空けているか確認
  - 外部接続するサーバーやローカルマシンにもredis-cli が入っていること

```
$ redis-cli -h {ipaddress} -p 6379 // ローカルマシンからコマンド入力
{ipaddress}:6379>  // 接続OK
```


